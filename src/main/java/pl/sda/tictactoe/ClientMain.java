package pl.sda.tictactoe;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] args) throws IOException {
        TcpConnection tcpConnection = new TcpConnection(new Socket("localhost", 12345));
        tcpConnection.setOnMessageReceived(System.out::println);
        tcpConnection.startListener();

        Scanner scanner = new Scanner(System.in);
        while (!Thread.interrupted()) {
            tcpConnection.sendMessage(scanner.nextLine());
        }
    }
}
