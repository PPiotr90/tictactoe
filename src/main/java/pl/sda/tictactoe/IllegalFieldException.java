package pl.sda.tictactoe;

public class IllegalFieldException extends  RuntimeException {
    public  IllegalFieldException(String s) {
        super(s);
    }
}
