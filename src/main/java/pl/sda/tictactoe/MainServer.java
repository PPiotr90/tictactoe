package pl.sda.tictactoe;

import java.io.IOException;

public class MainServer {
    public static void main(String[] args) throws IOException {
        TicTacToeServer ticTacToeServer = new TicTacToeServer(12345);
        ticTacToeServer.startServer();
    }
}
