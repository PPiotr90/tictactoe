package pl.sda.tictactoe;

public class TicTacToe {
    private  Field[][] game;
    Field nextField;

    public TicTacToe() {
        nextField = Field.X;
        game = new Field[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                game[i][j]= Field.EMPTY;
            }

        }
    }
    public  Field getVerticalWinner() {
        Field winner = null;
        for (int i = 0; i < 3; i++) {

        if(game[i][0]==game[i][2]&&game[i][2]==game[i][1]&& game[i][0]!=Field.EMPTY) {
            winner=game[i][0];
        }


        }
        return winner;
    }
    public  Field getHorizontalWinner() {
        Field winner = null;
        for (int i = 0; i < 3; i++) {

            if(game[0][i]==game[1][i]&&game[2][i]==game[0][i]&& game[0][i]!=Field.EMPTY) {
                winner=game[0][i];
            }


        }
        return winner;
    }
    public  Field getDiagonalWinner() {
        Field winner = null;


            if(game[0][0]==game[1][1]&&game[1][1]==game[2][2]&& game[0][0]!=Field.EMPTY) {
                winner=game[0][0];
            }
            else  if(game[0][2]==game[1][1]&&game[1][1]==game[2][0]&& game[0][2]!=Field.EMPTY) {
            winner=game[0][2];
        }

        return winner;
    }

    public  void  placeField(int x, int y, Field field) {
        if(game[x][y]!=Field.EMPTY || field!=nextField){
            throw new  IllegalFieldException(" illegal move!!");
        }
        else game[x][y] = field;
        if (field==Field.X) {
            nextField=Field.O;
        } else  nextField =Field.X;
    }

    @Override
    public String toString() {
        String result = new String();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                result+=", ";
                result+=fieldToString(game[j][i]);
            }
            result+="\n";

        }
        return  result;
    }
    private  String fieldToString(Field field) {

        switch (field) {
            case EMPTY:
                return "_";
            case X:
                return "X";
            case O:
                return "0";
        }
        return null;
    }
    public  String messageToSend() {
        return  "*platform"+this.toString().replace("\n", "");

    }
}
