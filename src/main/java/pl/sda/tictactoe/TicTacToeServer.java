package pl.sda.tictactoe;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TicTacToeServer {
   private TicTacToe ticTacToe;
    private ServerSocket serverSocket;
    private TcpConnection firstPlayerConnection;
    private TcpConnection secondPlayerConnection;
    private List<TcpConnection>  clientsList= new ArrayList<>();

    public TicTacToeServer(int i) throws IOException {
        serverSocket = new ServerSocket(i);
        ticTacToe = new TicTacToe();

    }

    public void startServer() throws IOException {
        while (!(serverSocket.isClosed())) {

            if (firstPlayerConnection == null) {
                Socket socket = serverSocket.accept();
                firstPlayerConnection = createClientConnection(socket);
                firstPlayerConnection.field=Field.X;
                clientsList.add(firstPlayerConnection);
                firstPlayerConnection.startListener();
                firstPlayerConnection.sendMessage("you play as: X");
                System.out.println("podłączono 1. gracza");

            } else if (secondPlayerConnection == null) {
                Socket socket = serverSocket.accept();
                secondPlayerConnection = createClientConnection(socket);
                secondPlayerConnection.field=Field.O;
                secondPlayerConnection.startListener();
                clientsList.add(secondPlayerConnection);
                secondPlayerConnection.sendMessage("you play as O");
                System.out.println("podłączono 2. gracza");
            }

        }
    }

    private TcpConnection createClientConnection(Socket socket) throws IOException {
        TcpConnection result = new TcpConnection(socket);
        result.setOnMessageReceived(message -> {
            String[] xy= message.split(",");
            int x = Integer.parseInt(xy[0]);
            int y = Integer.parseInt(xy[1]);
           try {
               ticTacToe.placeField(x, y, result.field);
           } catch (IllegalFieldException e) {
               result.sendMessage("Złe zagranie");
           }
           Field[] winners = new  Field[3];
           winners[0]= ticTacToe.getVerticalWinner();
           winners[1]= ticTacToe.getHorizontalWinner();
           winners[2]= ticTacToe.getDiagonalWinner();
            Field win=Arrays.stream(winners).filter(field -> field!=null).findFirst().orElse(null);

           clientsList.stream().forEach(tcpConnection -> tcpConnection.sendMessage("Plansza:\n"+ticTacToe.messageToSend()));
            if(win!=null)
                clientsList.stream().forEach(tcpConnection -> tcpConnection.sendMessage("winner is: "+win));
        });


        return result;
    }
}
