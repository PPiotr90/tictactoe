package pl.sda.tictactoe.fxml;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import pl.sda.tictactoe.TcpConnection;


import java.io.IOException;
import java.net.Socket;

public class Controller {
    @FXML
    GridPane gridPane;
    @FXML
    Label label;
    TcpConnection tcpConnection;
    Button[][] buttons;
public  void  initialize() throws IOException {
    buttons = new  Button[3][3];
    tcpConnection = new TcpConnection(new Socket("localhost", 12345));

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            Button button = new Button();


            String messageToSend = String.valueOf(i);
            messageToSend+=",";
            messageToSend+=String.valueOf(j);
            String finalMessageToSend = messageToSend;
            button.setOnAction(event -> tcpConnection.sendMessage(finalMessageToSend));
            buttons[i][j]= button;
            gridPane.add(button, i, j);
            tcpConnection.setOnMessageReceived(message -> {
                if(message.startsWith("*platform")) {
                    String messageSub = message.substring(11);
                    String[] fields = messageSub.split(", ");
                    for (int k = 0; k < fields.length; k++) {
                        if(fields[k]!="_") {
                            int x=k%3;
                            int y=(k-x)/3;
                            String value = fields[k];
                            Platform.runLater(() -> this.setButton(x, y, value));

                        }

                    }
                }
                else Platform.runLater(() -> label.setText(message));
            });
        }

    }
    tcpConnection.startListener();
}
private  void setButton(int x, int y, String text) {

    buttons[x][y].setText(text);
}
}

